"use strict";
const ctxt_pos = 0,
  ctxt_title = 1,
  ctxt_modal = 2,
  ctxt_prodlist = 3;

var sto = window.__sto,
  settings = require("./../../settings"),
  style = require("./main.css"),
  helper_methods = sto.utils.retailerMethod,
  custom = settings.custom,
  format = settings.format,
  creaidformat = settings.creaid,
  border_color = settings.border_color,
  productss = [],
  cta_type = settings.cta_type,
  placeholder = settings.format + "_" + settings.name + "_" + settings.creaid,
  classParent = "storetail-id-" + settings.format + settings.creaid,
  cta_size = settings.cta_size != '' ? settings.cta_size : 'small',
  isViewFired = false,
  isIMPfired = false,
  selectedProduct,
  flexInitt = true,
  selectedBtn = true,
  verifExistenceParent,
  verifExistenSize,
  isDisplayButtonFinished = sto.utils.deferred().promise;

var fontRoboto = document.createElement('link');
fontRoboto.setAttribute('rel', 'stylesheet');
fontRoboto.setAttribute('href', 'https://fonts.googleapis.com/css?family=Roboto');
var head = document.querySelector('head');
head.appendChild(fontRoboto);

// switch between img ou color background buttons
var classBGbtn = settings.buttons_bg_img != "" && settings.buttons_bg_hover != "" ? "sto-disable-bgcol" : "sto-disable-bgimg";
var classBGcta = settings.cta_bg != "" ? "sto-disable-bgcol" : "sto-disable-bgimg";

custom.sort(function (a, b) {
  return a[ctxt_pos] > b[ctxt_pos]
});

// Get all product ids from custom product organization
custom.forEach(function (e) {
  e[ctxt_prodlist].forEach(function (p) {
    productss.push(p);
  })
});

module.exports = {
  init: _init_(),
}

var formatSettings = {
    "ids": productss,
    "type": settings.format,
    "creaId": settings.creaid
  },
  format_setting = [{
    "id": "2222222",
    "type": "partner-card",
    "partner": {
      "name": "storetail",
      "crea_id": settings.creaid,
      "crea_type": "BF",
      "need_products": false,
      "class": "storetail storetail-bf-vignette storetail-id-" + settings.format + settings.creaid,
      "position_reference": null,
      "label": "butterfly"
    },
    "html": "<div class='sto-logo'></div><div class='sto-btn-container'></div>"
  }, {
    "id": "0000000000",
    "type": "partner-multi-products",
    "partner": {
      "name": "storetail",
      "crea_id": settings.creaid,
      "crea_type": "BF",
      "need_products": true,
      "class": "storetail storetail-bf storetail-bf-products",
      "position_reference": null,
    },
    "products": productss.map(function (pdt, i) {
      return {
        "id": pdt,
        "type": "partner-product",
        "partner": {
          "name": "storetail",
          "crea_id": settings.creaid,
          "crea_type": settings.format,
          "class": "storetail storetail-bf",
          "position_reference": null
        }
      }
    })
  }];



function _init_() {

  try {

    var loadData = __sto.getTrackers("BF").length === undefined ? "BF" : ["BF", settings.creaid];

    sto.load(loadData, function (tracker) {

      sto.utils.retailerMethod.crawlAPI(formatSettings).promise.then(function (result) {

        var avail = checkAvailability(result, settings.custom),
          ret = avail[1],
          relevantsProducts = avail[0];
          selectedProduct = relevantsProducts[0][0];

        if (typeof ret === "undefined") {
          helper_methods.createFormat(tracker, format_setting, result, 20, 1);
        } else {
          if (ret === true) {
            helper_methods.createFormat(tracker, format_setting, result, 20, 1, false);
          } else if (ret === false) {
            helper_methods.createFormat(tracker, format_setting, result, 20, 1, true);
            tracker.error({
              "tl": "mandatoryNotFound"
            });
            return removers;
          }
        }

        window.addEventListener("resize", function () {
          var visualNode = document.querySelector("." + classParent);
          if (visualNode) {
            var buttonsContainer = visualNode.querySelector(".sto-btn-container"),
              allButtons = buttonsContainer.querySelectorAll(".sto-btn." + classBGbtn);
            setTimeout(function () {
              updateStructure(visualNode, buttonsContainer, allButtons);
            }, 20);
          }
        });

        document.addEventListener("onFormatAddedInDOM", function (e) {
          try {
            var visualNodeContainer = e && e["detail"] && e["detail"]["formatNode"] ? e["detail"]["formatNode"] : null;

            if (visualNodeContainer && visualNodeContainer.className.indexOf(classParent) > -1) {
              var productsNodeContainer = visualNodeContainer.nextSibling,
                productsTile = productsNodeContainer.getElementsByClassName("product-card"),
                buttonsContainer = visualNodeContainer.querySelector(".sto-btn-container"),
                allButtons = [];

              // Linked product container to the visual node with data-crea attribute for selection purpose
              productsNodeContainer.setAttribute("data-crea", classParent);

              // Remove all content in the visual node
              visualNodeContainer.setAttribute("data-format", format);
              visualNodeContainer.setAttribute("data-tile", "0");
              visualNodeContainer.setAttribute("data-creaid", creaidformat);
              visualNodeContainer.setAttribute("nb-prods", relevantsProducts[1].length);
              visualNodeContainer.setAttribute("data-tile", "0");

              if (visualNodeContainer.style.display != "none" && isIMPfired === false) {
                tracker.display();
                isIMPfired = true;
              }

              if (isViewFired === false) {
                __sto.utils.addViewTracking("." + classParent, tracker);
                var int = window.setInterval(function() {
                  if (isElementInViewport(visualNodeContainer)) {
                    tc_events_20(null,"event",{
                      "event":"view_item",
                      "zone":"product-list",
                      "partenaire":"storetail",
                      "adtype":"BF",
                      "creation":settings.creaid,
                      "campagne":settings.oid,
                      "category":"storetail",
                      "action":"view-item",
                      "label":"butterfly",
                      "ean":relevantsProducts[0][0],
                      "name":sto.module.getProductData(relevantsProducts[0][0]).attributes.slug,
                      "brand":sto.module.getProductData(relevantsProducts[0][0]).attributes.brand,
                      "price":sto.module.getProductData(relevantsProducts[0][0]).attributes.price.price
                    });
                    window.clearInterval(int);
                  }
                }, 200);
                isViewFired = true;
              }

              style.use();

              // Create all buttons for all products
              buttonsContainer.innerHTML = "";
              for (var i = 0; i < relevantsProducts[1].length; i++) {
                if (productsTile.namedItem(relevantsProducts[0][i])) {
                  productsTile.namedItem(relevantsProducts[0][i]).setAttribute("data-tile", i);
                  var newButton = createButton(i, relevantsProducts);
                  newButton.addEventListener('click', function (e) {
                    selectProduct(tracker, this, visualNodeContainer, productsNodeContainer, productsTile);
                    visualNodeContainer.setAttribute("data-tile", this.getAttribute("data-tile"));
                  });
                  buttonsContainer.appendChild(newButton);
                  allButtons.push(newButton);
                }
              }

              var firstButton = allButtons[0].id;
              var firstDisplayedProd = '';
              Array.prototype.slice.call(productsTile).forEach(function(p){
                if(p.id == firstButton){firstDisplayedProd = p;}
              })

              tc_events_20(null,"event",{
                "event":"print_item",
                "zone":"product-list",
                "partenaire":"storetail",
                "adtype":"BF",
                "creation":settings.creaid,
                "campagne":settings.oid,
                "category":"storetail",
                "action":"print-item",
                "label":"butterfly",
                "ean":firstButton,
                "name":sto.module.getProductData(firstButton).attributes.slug,
                "brand":sto.module.getProductData(firstButton).attributes.brand,
                "price":sto.module.getProductData(firstButton).attributes.price.price
              });

              // Set index on each product tiles
              for (var i = 0; i < productsTile.length; i++) {
                if (productsTile[i].getAttribute("data-tile") === null) {
                  productsTile[i].setAttribute("data-tile", i)
                }
                productsTile[i].style.display = "none";
              }
              productsTile[0].style.display = "flex";

              if (settings.custom_images == true) {
                visualNodeContainer.setAttribute("data-bg", relevantsProducts[2][0]);
              }

              optionsLink(tracker, visualNodeContainer);
              updateStructure(visualNodeContainer, buttonsContainer, allButtons);
              document.querySelector('.sto-close').addEventListener('click', function () {
                tracker.close({
                  "tl": "closeBtn"
                });
                var elemVideo1 = document.querySelector(".sto-ecran");
                elemVideo1.parentNode.removeChild(elemVideo1);
                var elemVideo2 = document.querySelector(".sto-video");
                elemVideo2.parentNode.removeChild(elemVideo2);
              });
              document.querySelector('.sto-ecran').addEventListener('click', function () {
                tracker.close({
                  "tl": "blackScreen"
                });
                var elemVideo1 = document.querySelector(".sto-ecran");
                elemVideo1.parentNode.removeChild(elemVideo1);
                var elemVideo2 = document.querySelector(".sto-video");
                elemVideo2.parentNode.removeChild(elemVideo2);
              });

            }
          } catch (e) {
            //console.log({"3":e});
          }
        });
      });
    });
  } catch (e) {
    //console.log(e);
  }
}

function updateStructure(visualNodeContainer, buttonsContainer, allButtons) {
  if (window.innerWidth < "768") {
    resizeDiv(visualNodeContainer, 1242, 300);
    if (allButtons.length > 4) {
      // btns[4].style.display = "none";
    }
    buttonsContainer.style.marginTop = null;
    allButtons[0].style.marginTop = null;
    for (var i = 0; i < allButtons.length; i++) {
      allButtons[i].style.marginBottom = null;
    }
  } else {
    visualNodeContainer.style.height = null;
    if (allButtons[1]) {
      allButtons[1].style.marginTop = null;
    }
    btnCentreContainer(visualNodeContainer, buttonsContainer);
    if (allButtons.length > 4) {
      // btns[4].style.display = "block";
    }
  }

  ctacenter(visualNodeContainer);
}

function createButton(index, relevantsProducts) {
  var newButton = document.createElement("BUTTON");
  newButton.innerHTML = relevantsProducts[1][index];
  newButton.id = relevantsProducts[0][index];
  newButton.className = "sto-btn " + classBGbtn;
  newButton.setAttribute("data-theme", relevantsProducts[2][index]);
  newButton.setAttribute("data-tile", index);
  newButton.setAttribute("data-id", relevantsProducts[0][index]);
  newButton.setAttribute('data-state', 'unselected');

  if (selectedProduct == relevantsProducts[0][index]) {
    newButton.setAttribute('data-state', 'selected');
  }

  return newButton
}

function checkAvailability(crawledProds, products) {
  var realProducts = [],
    temporary = [
      [],
      [],
      []
    ],
    rowModal = 0,
    toggleModal = true,
    modalMode = false;

  products.forEach(function (thisProductLine, i) {
    var toggleRowProduct = true;
    thisProductLine[3].forEach(function (thisProduct, j) {
      if (toggleRowProduct === true && Object.keys(crawledProds).includes(thisProduct)) {
        rowModal++;
        toggleRowProduct = false;
        temporary[0].push(thisProduct);
        temporary[1].push(thisProductLine[1]);
        temporary[2].push(thisProductLine[0]);
      }
    });
    if (thisProductLine[2] == true) {
      modalMode = true;
      if (rowModal <= 0) {
        toggleModal = false;
      }
    }
    rowModal = 0;
  });
  realProducts[0] = temporary[0];
  realProducts[1] = temporary[1];
  realProducts[2] = temporary[2];
  return (modalMode == true ? toggleModal == false ? [realProducts, false] : [realProducts, true] : [realProducts])
}

function selectProduct(tracker, t, visualNodeContainer, productsContainers, productsTile) {
  var product = t.id;
  var index = t.getAttribute('data-id');
  tracker.browse({
    "tl": product,
    "pi": product
  })

  for (var i = 0; i < productsTile.length; i++) {
    if (productsTile[i].id === product) {
      productsTile[i].style.display = 'flex';
    } else {
      productsTile[i].style.display = 'none';
    }
  }
  for (var i = 0; i < visualNodeContainer.querySelectorAll('.sto-btn').length; i++) {
    visualNodeContainer.querySelectorAll('.sto-btn')[i].setAttribute('data-state', 'unselected')
  }
  t.setAttribute('data-state', 'selected');
  selectedProduct = t.id;

  if (settings.custom_images == true) {
    var bg = t.getAttribute('data-theme');
    visualNodeContainer.setAttribute("data-bg", bg);
  }

  tc_events_20(null,"event",{
    "event":"view_item",
    "zone":"product-list",
    "partenaire":"storetail",
    "adtype":"BF",
    "creation":settings.creaid,
    "campagne":settings.oid,
    "category":"storetail",
    "action":"view-item",
    "label":"butterfly",
    "ean":product,
    "name":sto.module.getProductData(product).attributes.slug,
    "brand":sto.module.getProductData(product).attributes.brand,
    "price":sto.module.getProductData(product).attributes.price.price
  });
}

function resizeDiv(cible, tw, th) {
  var telecomWidth = cible.offsetWidth;
  var widthRaport = ((telecomWidth + 15) * 100) / tw;
  cible.style.height = (((th + 4) * widthRaport) / 100) + 'px';
  //cible.style.height = ((th * telecomWidth) / tw) + 'px';
}

function btnCentreContainer(visualNodeContainer, btnCont) {
  var contain = visualNodeContainer.querySelector(".partner").offsetHeight;
  var btnContain = btnCont.offsetHeight;
  var ctaContain = visualNodeContainer.querySelector('.sto-options') ? visualNodeContainer.querySelector('.sto-options').offsetHeight : 0;
  var marginBtf = ((contain - btnContain - ctaContain) + 140) / 2;
  btnCont.style.top = parseInt(marginBtf) + "px";
}

function ctacenter(visualNodeContainer) {
  if (settings.cta_type && settings.cta_type != "" && settings.cta_type != "legal") {
    var ctaText = visualNodeContainer.querySelector(".sto-options .sto-options-cta").clientWidth;
    var widthTestCta = document.querySelector(".sto-options .sto-options-cta").clientWidth;
    var sizeFlech = ((10 * 100) / widthTestCta) + 1;
    var sizeText = (100 - sizeFlech) - 1.5;

    var ctaTextHeight = visualNodeContainer.querySelector(".sto-options-cta").clientHeight;
    var heightTestCta = visualNodeContainer.querySelector(".sto-options .sto-options-cta .sto-cta-text").clientHeight;
    visualNodeContainer.querySelector(".sto-options .sto-options-cta .sto-cta-text").style.marginTop = ((ctaTextHeight - heightTestCta) / 2) + "px";
  }
}

function optionsLink(tracker, visualNodeContainer) {
  var options = visualNodeContainer.querySelector('.sto-options');

  if (cta_type !== "none" && cta_type !== "" && !options) {
    var cta = document.createElement('DIV');
    cta.className = 'sto-options';
    cta.setAttribute('cta-type', cta_type);
    cta.innerHTML = '<div class="sto-options-cta" cta-size=' + cta_size + '>' + (settings.cta_icon != "" ? '<span class="sto-picto"></span>' : '') + '<span class="sto-cta-text">' + settings.cta_text + '</span></div>';
    //cta.innerHTML = '<div class="sto-options-cta" cta-size=' + cta_size + '><span class="sto-picto"></span><span class="sto-cta-text">' + settings.cta_text + '</span></div>';
    //settings.cta_icon == "" ? cta.querySelector(".sto-picto").style.display = 'none' :'';
    visualNodeContainer.appendChild(cta);
  }
  var link = visualNodeContainer.querySelector('.sto-options');
  switch (cta_type) {
    case 'redirection':
      link.addEventListener('click', function () {
        tracker.click();
        window.open(settings.cta_url, settings.cta_target);
      });
      visualNodeContainer.querySelector('.sto-logo').addEventListener('click', function () {
        tracker.click();
        window.open(settings.cta_url, settings.cta_target);
      });
      break;
    case 'file':
      link.addEventListener('click', function () {
        tracker.openPDF();
        var pdf = require("../../img/" + settings.cta_file);
        window.open(pdf, "_blank");
      });
      visualNodeContainer.querySelector('.sto-logo').addEventListener('click', function () {
        tracker.openPDF();
        var pdf = require("../../img/" + settings.cta_file);
        window.open(pdf, "_blank");
      });
      break;
    case 'video':
      link.addEventListener('click', function () {
        tracker.playVideo({
          "tl": "open"
        })
        var videoBackground = document.createElement('div');
        videoBackground.className = 'sto-ecran';
        var videoBox = document.createElement('div');
        videoBox.className = 'sto-video';
        videoBox.innerHTML = '<div class="sto-close"></div>' + settings.cta_video + '</div>';

        if (document.querySelectorAll(".sto-video").length < 1) {
            document.querySelector('body').appendChild(videoBackground);
            document.querySelector('body').appendChild(videoBox);
        }

        document.querySelector('.sto-close').addEventListener('click', function () {
          tracker.close({
            "tl": "closeBtn"
          });
          var elemVideo1 = document.querySelector(".sto-ecran");
          elemVideo1.parentNode.removeChild(elemVideo1);
          var elemVideo2 = document.querySelector(".sto-video");
          elemVideo2.parentNode.removeChild(elemVideo2);
        });
        document.querySelector('.sto-ecran').addEventListener('click', function () {
          tracker.close({
            "tl": "blackScreen"
          });
          var elemVideo1 = document.querySelector(".sto-ecran");
          elemVideo1.parentNode.removeChild(elemVideo1);
          var elemVideo2 = document.querySelector(".sto-video");
          elemVideo2.parentNode.removeChild(elemVideo2);
        });
      });
      visualNodeContainer.querySelector('.sto-logo').addEventListener('click', function () {
        tracker.playVideo({
          "tl": "open"
        })
        var videoBackground = document.createElement('div');
        videoBackground.className = 'sto-ecran';
        var videoBox = document.createElement('div');
        videoBox.className = 'sto-video';
        videoBox.innerHTML = '<div class="sto-close"></div>' + settings.cta_video + '</div>';


        if (document.querySelectorAll(".sto-video").length < 1) {
            document.querySelector('body').appendChild(videoBackground);
            document.querySelector('body').appendChild(videoBox);
        }
        document.querySelector('.sto-close').addEventListener('click', function () {
          tracker.close({
            "tl": "closeBtn"
          });
          var elemVideo1 = document.querySelector(".sto-ecran");
          elemVideo1.parentNode.removeChild(elemVideo1);
          var elemVideo2 = document.querySelector(".sto-video");
          elemVideo2.parentNode.removeChild(elemVideo2);
        });
        document.querySelector('.sto-ecran').addEventListener('click', function () {
          tracker.close({
            "tl": "blackScreen"
          });
          var elemVideo1 = document.querySelector(".sto-ecran");
          elemVideo1.parentNode.removeChild(elemVideo1);
          var elemVideo2 = document.querySelector(".sto-video");
          elemVideo2.parentNode.removeChild(elemVideo2);
        });
      });
      break;
    default:
  }
}

function isElementInViewport(el) {
  if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
    el = el[0];
  }
  var rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    !(rect.top == rect.bottom || rect.left == rect.right) &&
    !(rect.height == 0 || rect.width == 0) &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
    rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
  );
}
