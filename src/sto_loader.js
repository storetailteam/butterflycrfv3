var settings = require("./settings.json");
settings.btf_container_img = settings.btf_container_img === "" ? "none" : "url(./../../img/"+settings.btf_container_img+")";
settings.btf_container_img_mobile = settings.btf_container_img_mobile === "" ? "none" : "url(./../../img/"+settings.btf_container_img_mobile+")";
settings.logo_img = settings.logo_img === "" ? "none" : "url(./../../img/"+settings.logo_img+")";
settings.border_color = settings.border_color === "" ? "transparent" : settings.border_color;
settings.buttons_color = settings.buttons_color === "" ? "transparent" : settings.buttons_color;
settings.buttons_color_selected = settings.buttons_color_selected === "" ? "transparent" : settings.buttons_color_selected;
settings.buttons_bg_img = settings.buttons_bg_img === "" ? "none" : "url(./../../img/"+settings.buttons_bg_img+")";
settings.buttons_bg_img_hover = settings.buttons_bg_img_hover === "" ? "none" : "url(./../../img/"+settings.buttons_bg_img_hover+")";
settings.buttons_width = settings.buttons_width === "" ? "150px" : settings.buttons_width+"px";
settings.buttons_police_size = settings.buttons_police_size === "" ? "12" : settings.buttons_police_size;
settings.btf_color = settings.btf_color === "" ? "transparent" : settings.btf_color;
settings.buttons_txt_color = settings.buttons_txt_color === "" ? "transparent" : settings.buttons_txt_color;
settings.buttons_txt_color_color_selected = settings.buttons_txt_color_color_selected === "" ? "transparent" : settings.buttons_txt_color_color_selected;
settings.buttons_police_size = settings.buttons_police_size === "" ? "12" : settings.buttons_police_size;
settings.cta_color = settings.cta_color === "" ? "transparent" : settings.cta_color;
settings.cta_bg = settings.cta_bg === "" ? "none" : "url(./../../img/"+settings.cta_bg+")";
settings.cta_bg_color = settings.cta_bg_color === "" ? "transparent" : settings.cta_bg_color;
settings.cta_icon = settings.cta_icon === "" ? "none" : "url(./../../img/"+settings.cta_icon+")";
settings.container_img_01 = settings.container_img_01 === "" ? "none" : "url(./../../img/"+settings.container_img_01+")";
settings.container_img_02 = settings.container_img_02 === "" ? "none" : "url(./../../img/"+settings.container_img_02+")";
settings.container_img_03 = settings.container_img_03 === "" ? "none" : "url(./../../img/"+settings.container_img_03+")";
settings.container_img_04 = settings.container_img_04 === "" ? "none" : "url(./../../img/"+settings.container_img_04+")";
settings.container_img_05 = settings.container_img_05 === "" ? "none" : "url(./../../img/"+settings.container_img_05+")";

module.exports = function (source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, "storetail-id-"+settings.format+settings.creaid)
        .replace(/__FORMAT__/g, settings.format)
        .replace(/__LOGOIMG__/g,settings.logo_img)
        .replace(/__BTFCOLOR__/g,settings.btf_color)
        .replace(/__BORDERCOLOR__/g,settings.border_color)
        .replace(/__BUTTONTXT__/g,settings.buttons_txt_color)
        .replace(/__BUTTONTXTSELECTED__/g,settings.buttons_txt_color_selected)
        .replace(/__BUTTONCOLOR__/g,settings.buttons_color)
        .replace(/__BUTTONCOLORSELECTED__/g,settings.buttons_color_selected)
        .replace(/__BUTTON_BG_IMG__/g,settings.buttons_bg_img)
        .replace(/__BUTTON_BG_IMG_HOVER__/g,settings.buttons_bg_img_hover)
        .replace(/__BUTTONSWIDTH__/g,settings.buttons_width)
        .replace(/__BTN_POLICE_SIZE__/g,settings.buttons_police_size)
        .replace(/__BTF_CONTAINE_IMG__/g,settings.btf_container_img)
        .replace(/__BTF_CONTAINE_IMG_MOB__/g,settings.btf_container_img_mobile)
        .replace(/__CTACOLOR__/g,settings.cta_color)
        .replace(/__CTA_BG__/g,settings.cta_bg)
        .replace(/__CTA_BG_COLOR__/g,settings.cta_bg_color)
        .replace(/__CTA_ICON__/g,settings.cta_icon)
        .replace(/__BTF_BG_01__/g,settings.container_img_01)
        .replace(/__BTF_BG_02__/g,settings.container_img_02)
        .replace(/__BTF_BG_03__/g,settings.container_img_03)
        .replace(/__BTF_BG_04__/g,settings.container_img_04)
        .replace(/__BTF_BG_05__/g,settings.container_img_05);
    return test;
};
